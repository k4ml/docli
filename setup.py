from setuptools import setup, find_packages

setup(
    name="docli",
    version="0.1",
    packages=find_packages(),
    install_requires=[
        'Baker',
        'python-digitalocean',
    ],
    entry_points={
        'console_scripts': [
            'docli = docli:main',
        ],
    },
)
