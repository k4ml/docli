import os
import baker
import digitalocean

home_dir = os.path.expanduser('~')
token = open('%s/.python-do.rc' % home_dir).read().strip()
manager = digitalocean.Manager(token=token)

@baker.command
def list_droplets():
    my_droplets = manager.get_all_droplets()
    for droplet in my_droplets:
        print droplet.id, droplet.name, droplet.size, droplet.status
        print droplet.ip_address
        for event in droplet.get_events():
            print "\t", event.type, event.status

    return my_droplets

def get_droplet(name):
    droplets = list_droplets()
    for droplet in droplets:
        if droplet.name == name:
            return droplet

@baker.command
def poweroff_droplet(name):
    droplet = get_droplet(name)
    confirm = raw_input('Poweroff %s ? [Y/n]' % (name))
    if confirm == 'Y':
        droplet.power_off()

@baker.command
def poweron_droplet(name):
    droplet = get_droplet(name)
    confirm = raw_input('Poweron %s ? [Y/n]' % (name))
    if confirm == 'Y':
        droplet.power_on()

@baker.command
def resize_droplet(name, size, disk=False):
    droplet = get_droplet(name)
    confirm = raw_input('Resize %s to %s ? [Y/n]' % (name, size))
    if confirm == 'Y':
        droplet.resize(size, disk=disk)

def main():
    baker.run()

if __name__ == '__main__':
    main()
